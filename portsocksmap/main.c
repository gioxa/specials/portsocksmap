//
//  main.c
//  portsocksmap
//
//  Created by Danny Goossen on 24/4/2020.
//  Copyright © 2020 Danny Goossen. All rights reserved.
//

#include <stdio.h>
#include "connect_socks.h"
#include "error.h"
#include "network.h"

int main(int argc, const char * argv[]) {
	
	sockfd_t remfd=0;
	u_int8_t sess_stop=0;
	init_network();

	/* Socket pair allows to communicate to the forks and inform it's over*/
	sockfd_t                  sock_pair_rx[2];
	int status = socketpair(AF_UNIX,SOCK_DGRAM,0,sock_pair_rx);
	if ( status == SOCKET_ERROR ) {
		error(" socketpair(AF_UNIX, SOCK_STREAM, 0)\n");
	}
	struct sockaddr myaddr ,clientaddr;
	int sockid;
	sockid=socket(AF_INET,SOCK_STREAM,0);
	memset(&myaddr,'0',sizeof(myaddr));
	((struct sockaddr_in*)&myaddr)->sin_family=AF_INET;
	((struct sockaddr_in*)&myaddr)->sin_port=htons(15432);
	((struct sockaddr_in*)&myaddr)->sin_addr.s_addr=inet_addr("127.0.0.1");
	socket_blocking(sockid);
	
	if(sockid==-1)
	{
		perror("socket");
		exit(EXIT_FAILURE);
	}
	socklen_t len=sizeof(myaddr);
	if(bind(sockid,( struct sockaddr*)&myaddr,len)==-1)
	{
		perror("bind");
		exit(EXIT_FAILURE);
	}
	if(listen(sockid,10)==-1)
	{
		perror("listen");
		exit(EXIT_FAILURE);
	}
	int pid,new;
	static int counter=0;
	info("listening on 127.0.0.1:15432 to connect to meltano.deployctl.com:5432");
	for(;;)
	{
		new = accept(sockid, (struct sockaddr *)&clientaddr, &len);
		if (new==-1) continue;
		if ((pid = fork()) == -1)
		{
			close(new);
			continue;
		}
		else if(pid > 0)
		{
			close(new);
			counter++;
			printf("new fork started#:%d\n",counter);
			continue;
		}
		else if(pid == 0)
		{
			close(sock_pair_rx[1]);
			counter++;
			info("new connection w handle %d\n",new);
			remfd=connect_socks5("127.0.0.1",1080,"meltano.deployctl.com", 5432,20);
			if (remfd!=INVALID_SOCKET)
			{
				relaydata(&new, &remfd, sock_pair_rx[0] , &sess_stop);
				info("finish relay socket %d <-> %d\n",new,remfd);
			}
			else
				info("Problem connection to proxy server");
			close(new);
			close(remfd);
			exit(0);
		}
	}
	close(sockid);
	deinit_network();

	return 0;
}
