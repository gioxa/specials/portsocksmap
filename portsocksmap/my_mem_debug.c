//
//  my_mem_debug.c
//  tBlast_tunnel
//
//  Created by Danny Goossen on 24/2/15.
//  Copyright (c) 2015 Danny Goossen. All rights reserved.
//

#include "compat.h"
#include "my_mem_debug.h"


//#define PRINT_MEM


jmp_buf jump;

void segv (int sig)
{
    longjmp (jump, 1);
}

int illegalmem (void *x)
{
    volatile char c;
    int illegal = 0;
    
    signal (SIGSEGV, segv);
    
    if (!setjmp (jump))
        c = *(char *) (x);
    else
        illegal = 1;
    
    signal (SIGSEGV, SIG_DFL);
    
    return (illegal);
}

u_int32_t Memory_used;
static  pthread_mutex_t  mem_mutex;

void init_mem()
{
    
    /* create the tb_engine mutex */
    pthread_mutex_init(&mem_mutex, NULL);
    Memory_used=0;
    return;
}

u_int32_t get_mem_used()
{
    //pthread_mutex_lock(&mem_mutex);
    u_int32_t t=Memory_used;
    //pthread_mutex_unlock(&mem_mutex);
    return t;
}
void de_init_mem()
{
    pthread_mutex_destroy(&mem_mutex);
}
#ifndef NDEBUG

void * debugging_calloc (size_t nmemb, size_t size, const char *file,
                        unsigned long line)
{
    pthread_mutex_lock(&mem_mutex);
    void *ptr;
    size_t mm;
    
    assert (nmemb > 0);
    assert (size > 0);
    
    ptr = calloc (nmemb, size);
#ifdef __APPLE__
	mm=malloc_size(ptr);
#elif __ANDROID__
	mm=dlmalloc_usable_size(ptr);
#elif __MINGW32__
    mm=_msize(ptr);
#else
	mm=malloc_usable_size(ptr);
#endif
	
	
    Memory_used=Memory_used+(u_int32_t)mm;
#ifdef PRINT_MEM
    fprintf (stderr, "calloc: %p:%6lu :%s:%lu\n", ptr,
             mm, file, line);
#endif
    pthread_mutex_unlock(&mem_mutex);
    return ptr;
}

void *debugging_malloc (size_t size, const char *file, unsigned long line)
{
    pthread_mutex_lock(&mem_mutex);
    void *ptr;
     size_t mm;
    assert (size > 0);
   
    ptr = malloc (size);
	
#ifdef __APPLE__
    mm=malloc_size(ptr);
#elif __ANDROID__
	mm=dlmalloc_usable_size(ptr);
#elif __MINGW32__
    mm=_msize(ptr);
#else
    mm=malloc_usable_size(ptr);
#endif
	
	
    
    #ifdef PRINT_MEM
    fprintf (stderr, "malloc: %p:%6lu :%s:%lu\n", ptr,
             mm, file, line);
    #endif

    Memory_used=Memory_used+(u_int32_t)mm;
    pthread_mutex_unlock(&mem_mutex);
    return ptr;
}

void *debugging_realloc (void *ptr, size_t size, const char *file, unsigned long line)
{
    
    void *newptr;
    //void *orptr=ptr;
     pthread_mutex_lock(&mem_mutex);
    
#ifdef __APPLE__
   u_int32_t Memory_old=(u_int32_t)malloc_size(ptr);
#elif __ANDROID__
	u_int32_t Memory_old= dlmalloc_usable_size(ptr);
#elif __MINGW32__
    u_int32_t Memory_old= _msize(ptr);
#else
    u_int32_t Memory_old= malloc_usable_size(ptr);
#endif
	

	
    assert (size > 0);
   
    newptr = realloc (ptr, size);
    #ifdef PRINT_MEM
    fprintf (stderr, "{realloc: %p -> %p:%lu} %s:%lu\n", orptr, newptr,
             (unsigned long) size, file, line);
    #endif
   
    if (newptr)
    {
        Memory_used=Memory_used- Memory_old;  
        #ifdef __APPLE__
            Memory_used=Memory_used+ (u_int32_t)malloc_size(newptr);
        #elif __ANDROID__
		    Memory_used=Memory_used+ dlmalloc_usable_size(newptr);
        #elif __MINGW32__
            Memory_used=Memory_used+ _msize(newptr);
        #else
            Memory_used=Memory_used+ malloc_usable_size(newptr);
        #endif
        
    }
    
     pthread_mutex_unlock(&mem_mutex);
    return newptr;
}

void debugging_free (void *ptr, const char *file, unsigned long line)
{
    pthread_mutex_lock(&mem_mutex);
    size_t mm;
   
#ifdef __APPLE__
	mm=malloc_size(ptr);
#elif __ANDROID__
	mm=dlmalloc_usable_size(ptr);
#elif __MINGW32__
    mm=_msize(ptr);
#else
	mm=malloc_usable_size(ptr);
#endif

     if (ptr!=NULL) free (ptr);
    
#ifdef PRINT_MEM
    fprintf (stderr, "  free: %p:-%5lu :%s:%lu\n", ptr,mm, file, line);
#endif


    Memory_used=Memory_used- (u_int32_t)mm;

   
    pthread_mutex_unlock(&mem_mutex);
    return;
}

char *debugging_strdup (const char *s, const char *file, unsigned long line)
{
    char *ptr;
    size_t len;
    
    assert (s != NULL);
    
    len = strlen (s) + 1;
    ptr = (char *) malloc (len);
    if (!ptr)
        return NULL;
    memcpy (ptr, s, len);
    
    fprintf (stderr, "{strdup: %p:%lu} %s:%lu\n", ptr,
             (unsigned long) len, file, line);
    return ptr;
}

#endif /* !NDEBUG */