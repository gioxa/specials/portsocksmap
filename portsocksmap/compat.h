//
//  compat.h
//  tBlast_tunnel
//
//  Created by Danny Goossen on 5/6/16.
//  Copyright (c) 2016 Danny Goossen. All rights reserved.
//

#ifndef tBlast_tunnel_compat_h
#define tBlast_tunnel_compat_h

#define _GNU_SOURCE
#include <pthread.h>
#include <sys/types.h>
#include <stdint.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/file.h>
#include <ctype.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

#include <stdarg.h>
#include <limits.h>

#include <signal.h>
#include <getopt.h>      /* for getopt_long()                     */
#include <setjmp.h>
#include <assert.h>
#include <time.h>
#include <sys/time.h>
#include <inttypes.h>

#ifndef min
#define min(a,b)  (((a) < (b)) ? (a) : (b))
#endif
#ifndef max
#define max(a,b)  (((a) > (b)) ? (a) : (b))
#endif

#ifndef __linux__
#ifdef ntohll
#define HAVE_HTONLL 1
#endif
#endif



#ifdef __MINGW32__
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <w32api.h>
	#include <windows.h>
	#include <io.h>
//	#define _POSIX_C_SOURCE 199309L

	typedef uint8_t  u_int8_t;
	typedef uint16_t  u_int16_t;
	typedef uint32_t  u_int32_t;
	typedef uint64_t  u_int64_t;

struct iovec {
    void *iov_base;   /* Starting address */
    size_t iov_len;   /* Number of bytes */
};
struct msghdr {
	void		*msg_name;	/* [XSI] optional address */
	socklen_t	msg_namelen;	/* [XSI] size of address */
	struct		iovec *msg_iov;	/* [XSI] scatter/gather array */
	int		msg_iovlen;	/* [XSI] # elements in msg_iov */
	void		*msg_control;	/* [XSI] ancillary data, see below */
	socklen_t	msg_controllen;	/* [XSI] ancillary data buffer len */
	int		msg_flags;	/* [XSI] flags on received message */
};



inline static int getdtablesize(void){ return 2048; }

	// syslog dummies for win


#ifndef HAVE_SYSLOG_H
#define LOG_PID 1
#define LOG_USER 1
#define LOG_NOTICE 1
#define LOG_ERR 1
inline static void closelog (void){return;}
inline static void syslog (int i, char * s, ...){return;}
inline static void vsyslog(int priority, const char *format, va_list ap){return;}
inline static void init_syslog(char * address){return;}
inline static void openlog (char * s,int i, int j ){return;}
inline static void exit_syslog (void ){return;}
#else
#include <syslog.h>
#endif


	// # temp solution, need to find something for this
	//inline static int errno_sock() {return  WSAGetLastError(); }
	#define errno_sock WSAGetLastError()
	typedef SOCKET sockfd_t;
//	ssize_t	sendmsg(sockfd_t fd, const struct msghdr * message, int flags);

	inline static void close_socket(sockfd_t socket) { closesocket(socket); }

	#ifndef HAVE_SENDMSG
	ssize_t	sendmsg(sockfd_t fd, const struct msghdr * message, int flags);
	#endif

	// included ctime

	#define CLOCK_REALTIME 0



  void hnsleep(__int64 usec);
	void tb_usleep (__int64 usec);

	#ifndef _TIMEVAL_DEFINED // also in winsock[2].h
		#define _TIMEVAL_DEFINED
		struct timeval {
		  long tv_sec;
		  long tv_usec;
		};
		#define timerisset(tvp)	 ((tvp)->tv_sec || (tvp)->tv_usec)
		#define	timercmp(tvp, uvp, cmp) \
			(((tvp)->tv_sec == (uvp)->tv_sec) ? \
			    ((tvp)->tv_usec cmp (uvp)->tv_usec) :	\
			    ((tvp)->tv_sec cmp (uvp)->tv_sec))

		#define timerclear(tvp)	 (tvp)->tv_sec = (tvp)->tv_usec = 0

		struct timezone
			{
			  int tz_minuteswest; // of Greenwich
			  int tz_dsttime;     // type of dst correction to apply
			};

	#endif // _TIMEVAL_DEFINED


//	int clock_gettime(int X, struct timespec *spec);

//#ifndef HAVE_NANOSLEEP
//	int nanosleep(const struct timespec *request, struct timespec *remain);
//#endif
#ifndef HAVE_GETTIMEOFDAY
	inline int gettimeofday(struct timeval* p, void* tz ) //tz IGNORED
	{
			union {
					long long ns100; // time since 1 Jan 1601 in 100ns units
					FILETIME ft;
			} now;
			//printf("going to get windows time\n");
			GetSystemTimeAsFileTime( &(now.ft) );
			p->tv_usec=(long)((now.ns100 / 10LL) % 1000000LL );
			p->tv_sec= (long)((now.ns100-(116444736000000000LL))/10000000LL);
			//printf("got windows time\n");
			return 0;
	}
#endif


	int socketpair(int domain, int type, int protocol, sockfd_t fds[2]);
	int inet_pton(int af, const char *src, void *dst);

	#ifndef IPV6_V6ONLY
	#define IPV6_V6ONLY 27
	#endif

#else // Not __MINGW32__

	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <netdb.h>
	#include <netinet/in.h>
	#include <syslog.h>
  #include <net/if.h>

	inline static void close_socket(int socket){close(socket);return;}
	#define INVALID_SOCKET -1
	#define SOCKET_ERROR -1
	#include <errno.h>
	#define errno_sock errno
	typedef int sockfd_t;
	#define tb_usleep usleep
#endif

// need after declarations of integers

#include "util.h"

#ifndef HAVE_HTONLL
extern u_int64_t htonll(u_int64_t);
extern u_int64_t ntohll(u_int64_t);
#endif


#ifndef IN_PRIVATE

#define	IN_PRIVATE(i)	((((u_int32_t)(i) & 0xff000000) == 0x0a000000) || \
(((u_int32_t)(i) & 0xfff00000) == 0xac100000) || \
(((u_int32_t)(i) & 0xffff0000) == 0xc0a80000))

#endif

#ifndef IN_LOCAL_GROUP
#define	IN_LOCAL_GROUP(i)	(((u_int32_t)(i) & 0xffffff00) == 0xe0000000)
#endif

#ifndef IN_CLASSB_NET
#define	IN_CLASSB_NET		0xffff0000
#endif

#ifndef IN_LINKLOCALNETNUM
#define IN_LINKLOCALNETNUM	(u_int32_t)0xA9FE0000 /* 169.254.0.0 */
#endif

#ifndef IN_LINKLOCAL
#define IN_LINKLOCAL(i)		(((u_int32_t)(i) & IN_CLASSB_NET) == IN_LINKLOCALNETNUM)
#endif


#ifndef IN_ANY_LOCAL
#define	IN_ANY_LOCAL(i)		(IN_LINKLOCAL(i) || IN_LOCAL_GROUP(i))
#endif

#ifndef SHUT_RDWR
#define SHUT_RDWR SD_BOTH
#define SHUT_WR   SD_SEND
#define SHUT_RD   SD_RECEIVE
#endif


//android
#ifndef HAVE_LOCKF
#if HAVE_FLOCK==1
// The lockf() function is not available on Android; we translate to flock().
#define F_TLOCK LOCK_EX
#define F_ULOCK LOCK_UN
static inline int lockf(int fd, int cmd, off_t ignored_len);
#endif
#endif

//hack for mac os and windows
#ifndef MSG_NOSIGNAL
#    define MSG_NOSIGNAL 0
#endif

// HACK for MinGW.
#ifndef EAFNOSUPPORT
#   define EAFNOSUPPORT 1
#endif

// HACK for MinGW. Flags, can do more here!!!
#ifndef O_NONBLOCK
#   define O_NONBLOCK 04000
#endif

#ifndef NS_INADDRSZ
  #define NS_INADDRSZ  4
  #define NS_IN6ADDRSZ 16
  #define NS_INT16SZ   2
#endif

#ifndef HAVE_INET_PTON4
int inet_pton4(const char *src, char *dst);
#endif

#ifndef HAVE_INET_PTON6
int inet_pton6(const char *src, char *dst);
#endif

//errno_sock the numbers
#ifdef __MINGW32__

#ifndef ERANGE
#define ERANGE 34
#endif

	#define SCK_EINTR                   WSAEINTR
	#define SCK_EWOULDBLOCK             WSAEWOULDBLOCK
	#define SCK_ESHUTDOWN               WSAESHUTDOWN
	#define SCK_EINPROGRESS             WSAEINPROGRESS
	#define SCK_EALREADY                WSAEALREADY
	#define SCK_ENOTSOCK                WSAENOTSOCK
	#define SCK_EDESTADDRREQ            WSAEDESTADDRREQ
	#define SCK_EMSGSIZE                WSAEMSGSIZE
	#define SCK_EPROTOTYPE              WSAEPROTOTYPE
	#define SCK_ENOPROTOOPT             WSAENOPROTOOPT
	#define SCK_EPROTONOSUPPORT         WSAEPROTONOSUPPORT
	#define SCK_ESOCKTNOSUPPORT         WSAESOCKTNOSUPPORT
	#define SCK_EOPNOTSUPP              WSAEOPNOTSUPP
	#define SCK_EPFNOSUPPORT            WSAEPFNOSUPPORT
	#define SCK_EAFNOSUPPORT            WSAEAFNOSUPPORT
	#define SCK_EADDRINUSE              WSAEADDRINUSE
	#define SCK_EADDRNOTAVAIL           WSAEADDRNOTAVAIL
	#define SCK_ENETDOWN                WSAENETDOWN
	#define SCK_ENETUNREACH             WSAENETUNREACH
	#define SCK_ENETRESET               WSAENETRESET
	#define SCK_ECONNABORTED            WSAECONNABORTED
	#define SCK_ECONNRESET              WSAECONNRESET
	#define SCK_ENOBUFS                 WSAENOBUFS
	#define SCK_EISCONN                 WSAEISCONN
	#define SCK_ENOTCONN                WSAENOTCONN
	#define SCK_ETOOMANYREFS            WSAETOOMANYREFS
	#define SCK_ETIMEDOUT               WSAETIMEDOUT
	#define SCK_ECONNREFUSED            WSAECONNREFUSED
	#define SCK_ELOOP                   WSAELOOP
	#define SCK_EHOSTDOWN               WSAEHOSTDOWN
	#define SCK_EHOSTUNREACH            WSAEHOSTUNREACH
	#define SCK_EPROCLIM                WSAEPROCLIM
	#define SCK_EUSERS                  WSAEUSERS
	#define SCK_EDQUOT                  WSAEDQUOT
	#define SCK_ESTALE                  WSAESTALE
	#define SCK_EREMOTE                 WSAEREMOTE
	#define SCK_ENAMETOOLONG            WSAENAMETOOLONG
	#define SCK_ENOTEMPTY               WSAENOTEMPTY
	#define SCK_EBADF										WSAEBADF
//  10009 (WSAEBADF):

	/* GAI error codes */
	# ifndef EAI_BADFLAGS
	#  define EAI_BADFLAGS -1
	# endif
	# ifndef EAI_NONAME
	#  define EAI_NONAME -2
	# endif
	# ifndef EAI_AGAIN
	#  define EAI_AGAIN -3
	# endif
	# ifndef EAI_FAIL
	#  define EAI_FAIL -4
	# endif
	# ifndef EAI_NODATA
	#  define EAI_NODATA -5
	# endif
	# ifndef EAI_FAMILY
	#  define EAI_FAMILY -6
	# endif
	# ifndef EAI_SOCKTYPE
	#  define EAI_SOCKTYPE -7
	# endif
	# ifndef EAI_SERVICE
	#  define EAI_SERVICE -8
	# endif
	# ifndef EAI_ADDRFAMILY
	#  define EAI_ADDRFAMILY -9
	# endif
	# ifndef EAI_MEMORY
	#  define EAI_MEMORY -10
	# endif
	#ifndef EAI_OVERFLOW
	#  define EAI_OVERFLOW -11
	#endif
	# ifndef EAI_SYSTEM
	#  define EAI_SYSTEM -12
	# endif
#ifndef NO_ADDRESS
#   define NO_ADDRESS  NO_DATA
#endif
#ifndef INADDR_NONE
#   define INADDR_NONE 0xFFFFFFFF
#endif
#ifndef AF_UNSPEC
#   define AF_UNSPEC   0
#endif

//#define	EMFILE		24		/* Too many open files */

#ifndef s6_addr32
#define s6_addr32 __u6_addr.__u6_addr32
#endif

#ifndef HAVE_GAI_STRERROR
static const struct
{
    int        code;
    const char msg[41];
} gai_errlist[] =
{
    { 0,              "Error 0" },
    { EAI_BADFLAGS,   "Invalid flag used" },
    { EAI_NONAME,     "Host or service not found" },
    { EAI_AGAIN,      "Temporary name service failure" },
    { EAI_FAIL,       "Non-recoverable name service failure" },
    { EAI_NODATA,     "No data for host name" },
    { EAI_FAMILY,     "Unsupported address family" },
    { EAI_SOCKTYPE,   "Unsupported socket type" },
    { EAI_SERVICE,    "Incompatible service for socket type" },
    { EAI_ADDRFAMILY, "Unavailable address family for host name" },
    { EAI_MEMORY,     "Memory allocation failure" },
    { EAI_OVERFLOW,   "Buffer overflow" },
    { EAI_SYSTEM,     "System error" },
    { 0,              "" },
};
#endif
#ifndef AI_ALL
  #define AI_ALL                    0x0100
#endif
#ifndef AI_ADDRCONFIG
  #define AI_ADDRCONFIG             0x0400
#endif
#ifndef AI_V4MAPPED
  #define AI_V4MAPPED               0x0800
#endif

// stub for windows set name
#ifndef HAVE_PTHREAD_SETNAME_NP
#define pthread_setname_np(x,y) 1
#endif
#else // POSIX's

#define SCK_EINTR                   EINTR
#ifdef EWOULDBLOCK
#define SCK_EWOULDBLOCK           EWOULDBLOCK
#else
#define SCK_EWOULDBLOCK           EAGAIN
#endif
#define SCK_ESHUTDOWN               ESHUTDOWN
#define SCK_EINPROGRESS             EINPROGRESS
#define SCK_EALREADY                EALREADY
#define SCK_ENOTSOCK                ENOTSOCK
#define SCK_EDESTADDRREQ            EDESTADDRREQ
#define SCK_EMSGSIZE                EMSGSIZE
#define SCK_EPROTOTYPE              EPROTOTYPE
#define SCK_ENOPROTOOPT             ENOPROTOOPT
#define SCK_EPROTONOSUPPORT         EPROTONOSUPPORT
#define SCK_ESOCKTNOSUPPORT         ESOCKTNOSUPPORT
#define SCK_EOPNOTSUPP              EOPNOTSUPP
#define SCK_EPFNOSUPPORT            EPFNOSUPPORT
#define SCK_EAFNOSUPPORT            EAFNOSUPPORT
#define SCK_EADDRINUSE              EADDRINUSE
#define SCK_EADDRNOTAVAIL           EADDRNOTAVAIL
#define SCK_ENETDOWN                ENETDOWN
#define SCK_ENETUNREACH             ENETUNREACH
#define SCK_ENETRESET               ENETRESET
#define SCK_ECONNABORTED            ECONNABORTED
#define SCK_ECONNRESET              ECONNRESET
#define SCK_ENOBUFS                 ENOBUFS
#define SCK_EISCONN                 EISCONN
#define SCK_ENOTCONN                ENOTCONN
#define SCK_ETOOMANYREFS            ETOOMANYREFS
#define SCK_ETIMEDOUT               ETIMEDOUT
#define SCK_ECONNREFUSED            ECONNREFUSED
#define SCK_ELOOP                   ELOOP
#define SCK_EHOSTDOWN               EHOSTDOWN
#define SCK_EHOSTUNREACH            EHOSTUNREACH
#define SCK_EPROCLIM                EPROCLIM
#define SCK_EUSERS                  EUSERS
#define SCK_EDQUOT                  EDQUOT
#define SCK_ESTALE                  ESTALE
#define SCK_EREMOTE                 EREMOTE
#define SCK_ENAMETOOLONG            ENAMETOOLONG
#define SCK_ENOTEMPTY               ENOTEMPTY
#define SCK_EBADF		 								EBADF

#endif


#ifndef s6_addr
#define	s6_addr   __u6_addr.__u6_addr8
#endif

#ifndef INET6_ADDRSTRLEN
#define	INET6_ADDRSTRLEN	46
#endif

#ifndef IN6_IS_ADDR_V4MAPPED
#define	IN6_IS_ADDR_V4MAPPED(a)		      \
	((*(const uint32_t *)(const void *)(&(a)->s6_addr[0]) == 0) && \
	(*(const uint32_t *)(const void *)(&(a)->s6_addr[4]) == 0) && \
	(*(const uint32_t *)(const void *)(&(a)->s6_addr[8]) == \
	ntohl(0x0000ffff)))

#endif


#ifdef __APPLE__
	#include <arpa/inet.h>
	#include <mach/mach_time.h>
	#define SOL_IP IPPROTO_IP
#ifndef IP_RECVTOS
#define IP_RECVTOS IP_RECVOPTS
#endif
#endif


#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#ifndef __APPLE__
#ifndef __MINGW32__
	#include <linux/sockios.h> /* For the SIOCINQ / FIONREAD ioctl */
    #include <linux/errqueue.h>
	//#include <netinet/ip_icmp.h>
 	#include <linux/icmp.h>
#endif
    #include <time.h>

#endif

#ifdef LLONG_MIN
#ifndef LONG_LONG_MIN
#define LONG_LONG_MIN LLONG_MIN
#endif
#endif
#ifdef LLONG_MAX
#ifndef LONG_LONG_MAX
#define LONG_LONG_MAX LLONG_MAX
#endif
#endif

#ifdef _WIN32
#ifdef _MSC_VER
typedef signed   __int64    Int64_t;
typedef unsigned __int64    Uint64_t;
#else
typedef signed   long long  Int64_t;
typedef unsigned long long  Uint64_t;
#endif
#endif

#ifdef __MINGW32__
#include <io.h>
#ifndef HAVE_FSEEKO
#define ftello ftell
#define fseeko fseek
#endif

#ifndef HAVE_CLOCK_GETTIME
/*
static inline int clock_gettime(int X, struct timespec *spec)      //C-file part
{  __int64 wintime; GetSystemTimeAsFileTime((FILETIME*)&wintime);
   wintime      -=(long long)116444736000000000;  //1jan1601 to 1jan1970
   spec->tv_sec  =wintime / (long long)10000000;           //seconds
   spec->tv_nsec =wintime % (long long)10000000 *100;      //nano-seconds
   return 0;
}
*/
int clock_gettime(int X, struct timespec *spec);
#define HAVE_CLOCK_GETTIME 1
#endif


#ifndef HAVE_BZERO
  static inline void bzero(void * ptr, size_t len){ memset(ptr, 0, len); return;}
  #define HAVE_BZERO 1
#endif

#endif  // def __MINGW32__

//android
#ifndef __APPLE__
#ifndef __MINGW32__
 #ifndef HAVE_LOCKF
  static inline int lockf(int fd, int cmd, off_t ignored_len){ return flock(fd, cmd); }
  #define HAVE_LOCKF 1
 #endif
#endif
#endif

//#if (defined (WIN32) || defined (_WIN32))
//extern int __cdecl _fseeki64(FILE *, Int64_t, int);
//#extern Int64_t __cdecl _ftelli64(FILE *);

//#define fseeko _fseeki64
//#define ftello tell

//#endif









#endif /* defined(__livestreamer__compat__) */
