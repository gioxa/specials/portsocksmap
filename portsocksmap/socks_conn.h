//
//  socks_conn.h
//  tBlast_tunnel
//
//  Created by Danny Goossen on 29/7/15.
//  Copyright (c) 2015 Danny Goossen. All rights reserved.
//

#ifndef __tBlast_tunnel__socks_conn__
#define __tBlast_tunnel__socks_conn__

//#include <stdio.h>
#define SOCK_ERR_MASK   0x70
#define SOCK_IDLE       0x00
#define SOCK_HELO_RCVD  0x01
#define SOCK_HELO_ERR   0x72
#define SOCK_HELO_ASWRD 0x02
#define SOCK_REQ_RECV   0x03
#define SOCK_REQ_W4RC   0x03 // wait for remote connect

#define SOCK_REQ_ERR    0x77
#define SOCK_REQ_TIMEOUT 0x74
#define SOCK_ACTIVE_TCP 0x07
#define SOCK_ERR_SEND  0x73


#define SOCK_ATYP_POS   0x03
#define SOCK_ATYP_IP4   0x01
#define SOCK_ATYP_IP6   0x04
#define SOCK_ATYP_DOM   0x03


#define SOCK_CMD_CONNECT   0x01
#define SOCK_CMD_BIND      0x02
#define SOCK_CMD_UDP       0x03
#define HTTP_CMD_CONECT    0x04

#define SOCK_CMD_OFFSET    0x01

#define SOCK_REP_SUCCEED           0x00
#define SOCK_REP_SOCKS_FAILURE     0x01
#define SOCK_REP_CON_NOT_ALLOWED   0x02
#define SOCK_REP_NETW_UNREACHABLE  0x03
#define SOCK_REP_HOST_UNREACHABLE  0x04
#define SOCK_REP_CON_REFUSED       0x05
#define SOCK_REP_TTL_EXPIRED       0x06
#define SOCK_REP_CMD_UNSUPPORTED   0x07
#define SOCK_REP_ADD_UNSUPPORTED   0x08



#endif /* defined(__tBlast_tunnel__socks_conn__) */
