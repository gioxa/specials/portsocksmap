//
//  network.h
//  portsocksmap
//
//  Created by Danny Goossen on 24/4/2020.
//  Copyright © 2020 Danny Goossen. All rights reserved.
//

#ifndef network_h
#define network_h

#include "compat.h"

#define NET_ADDR_SIZE sizeof(struct sockaddr_storage)

int socket_nonblocking (sockfd_t sock);
int socket_blocking (sockfd_t sock);
int socket_nonblocking_no_keep_alive (sockfd_t sock);

//char *get_ip_string (struct sockaddr *sa, char *buf, size_t buflen);


int write_message (sockfd_t fd, const char *fmt, ...);

void relaydata(sockfd_t *client_fd, sockfd_t *rem_fd, sockfd_t signalfd , u_int8_t * sess_stop);


void init_network(void);
void deinit_network(void);

#endif /* network_h */
