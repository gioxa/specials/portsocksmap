//
//  my_mem_debug.h
//  tBlast_tunnel
//
//  Created by Danny Goossen on 24/2/15.
//  Copyright (c) 2015 Danny Goossen. All rights reserved.
//

#ifndef __tBlast_tunnel__my_mem_debug__
#define __tBlast_tunnel__my_mem_debug__

#include "compat.h"
#include "util.h"

//#define NDEBUG

/*
 * The following is to allow for better memory checking.
 */
void init_mem();
void de_init_mem();
int illegalmem (void *x);
extern u_int32_t Memory_used;
u_int32_t get_mem_used();

#ifndef NDEBUG

extern void *debugging_calloc (size_t nmemb, size_t size, const char *file,
                               unsigned long line);
extern void *debugging_malloc (size_t size, const char *file,
                               unsigned long line);
extern void debugging_free (void *ptr, const char *file, unsigned long line);
extern void *debugging_realloc (void *ptr, size_t size, const char *file,
                                unsigned long line);
extern char *debugging_strdup (const char *s, const char *file,
                               unsigned long line);

#  define safecalloc(x, y) debugging_calloc(x, y, __FILE__, __LINE__)
#  define safemalloc(x) debugging_malloc(x, __FILE__, __LINE__)
#  define saferealloc(x, y) debugging_realloc(x, y, __FILE__, __LINE__)
#  define safestrdup(x) debugging_strdup(x, __FILE__, __LINE__)
#  define safefree(x) (debugging_free(x, __FILE__, __LINE__), *(&(x)) = NULL)

#else

#  define safecalloc(x, y) calloc(x, y)
#  define safemalloc(x) malloc(x)
#  define saferealloc(x, y) realloc(x, y)
#  define safefree(x) (free (x), *(&(x)) = NULL)
#  define safestrdup(x) strdup(x)

#endif

#endif /* defined(__tBlast_tunnel__my_mem_debug__) */
